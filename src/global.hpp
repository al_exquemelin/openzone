/*  openZone - global.hpp - global constants definitions
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

//"screen" size (in fact, the window's one)
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;

//sleep time between frames
const int FRAME_DELAY = 40;

extern SDL_Window* window;
extern SDL_Surface* screenSurface;
extern SDL_Renderer* renderer;

extern SDL_Color c_black, c_white, c_darkgray, c_lightgray, c_darkmagenta, c_darkyellow,
	   c_lightyellow, c_darkred, c_lightred, c_lightgreen;

//headings
enum hdgs {N, NE, E, SE, S, SW, W, NW};

struct fPoint
{
	float x, y;
};
