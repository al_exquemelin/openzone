/*  openZone - openzone.cpp - the main file
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstddef>
#include <SDL2/SDL.h>
#include "game.hpp"
#include "global.hpp"
using namespace std;

SDL_Window* window = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Renderer* renderer = NULL;

//init the colours
SDL_Color c_black = {.r = 0x00, .g = 0x00, .b = 0x00, .a = 0xff},
		  c_white = {.r = 0xff, .g = 0xff, .b = 0xff, .a = 0xff},
		  c_darkgray = {.r = 0x33, .g = 0x33, .b = 0x33, .a = 0xff},
		  c_lightgray = {.r = 0x66, .g = 0x66, .b = 0x66, .a = 0xff},
		  c_darkmagenta = {.r = 0x33, .g = 0x00, .b = 0x77, .a = 0xff},
		  c_darkyellow = {.r = 0xdd, .g = 0xbb, .b = 0x00, .a = 0xff},
		  c_lightyellow = {.r = 0xff, .g = 0xff, .b = 0xaa, .a = 0xff},
		  c_darkred = {.r = 0xaa, .g = 0x00, .b = 0x00, .a = 0xff},
		  c_lightred = {.r = 0xee, .g = 0x00, .b = 0x00, .a = 0xff},
		  c_lightgreen = {.r = 0x33, .g = 0xaa, .b = 0x33, .a = 0xff};

int main(int argc, char* args[])
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
	}
	else
	{
		window = SDL_CreateWindow("openZone", SDL_WINDOWPOS_UNDEFINED, 
				SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 
				SDL_WINDOW_SHOWN);
		if(window == NULL)
		{
			cout << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
		}
		else
		{
			screenSurface = SDL_GetWindowSurface(window);
			//init the renderer
			renderer = SDL_CreateRenderer(window, -1, 
					SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if(renderer == NULL)
			{
				cout << "Renderer could not be created! SDL_Error: " << SDL_GetError() << endl;
			}
			else
			{
				SDL_RenderClear(renderer);
				test_game();
			}

		}
	}

	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
