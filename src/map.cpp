/*  openZone - map.cpp - defining and drawing terrain
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
#include "game.hpp"
#include "global.hpp"
#include "map.hpp"
#include "point.hpp"
using namespace std;

struct Map map = {.w = 6400, .h = 6400};

void draw_map_border()
{
	//draws that imaginary border deflecting the planes which get near it
	Point st(0, 0), fi(0, 0);
	if(cam.x < 0)
	{
		//left border is visible
	}
	else if(cam.x + SCREEN_WIDTH > map.w)
	{
		//right border is visible
	}
}
