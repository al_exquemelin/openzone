/*  openZone - point.hpp - the header file for the Point class
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#pragma once

std::vector<fPoint> mirror_vec(std::vector<fPoint> vec);

class Point
{
	//a class for points with integer coordinates
	public:
		Sint16 x, y;
		//the constructor
		Point(Sint16 x, Sint16 y):
			x(x), y(y)
		{}
		
		void rotate(float hdg, unsigned short x0, unsigned short y0)
		{
			//updates coordinates of the point when turning around (x0, y0)
			//the angle, measured from N, Pi/4 is 45 degrees
			float th = (M_PI / 4) * hdg;
			//the equations are from the rotation matrix
			Sint16 x_ = (Sint16)(x0 + x * cosf(th) - y * sinf(th)),
				y_ = (Sint16)(y0 + x * sinf(th) + y * cosf(th));
			x = x_;
			y = y_;
		}
};
