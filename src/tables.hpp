/*  openZone - tables.hpp - the header file of lookup tables
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <SDL2/SDL.h>
#include "global.hpp"

//body points lookup table
//consists of fPoint structs, the upper dimension is enum'd by plane type
std::vector<std::vector<fPoint>> body_t = {
	//GHOST
	{{0, 4.3}, {3.4, 4.3}, {5.6, 5.8}, {6.9, 5.2}, {13.6, 9}, {18.5, 9}, {20.4, 7.3},
	{20.2, 5.6}, {16.2, 1.1}, {14.3, -1.3}, {13.7, -1}, {8.5, -4.4}, {6.5, -6.1},
	{5.1, -5.3}, {0, -7.5}},
	{{1, 1}, {1, 2}, {1, 1}}
};
//canopy points table
std::vector<std::vector<fPoint>> canopy_t = {
	//GHOST
	{{0, -3.3}, {3, 0}, {2.6, 2.5}, {0, 2}}
};
//engine points table
std::vector<std::vector<fPoint>> engine_t = {{{0, 1}}};
//colours for bodys and canopys, indexed by planetype
SDL_Color body_cols[] = {c_darkmagenta},
		  canopy_cols[] = {c_lightgray};
