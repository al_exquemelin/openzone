/*  openZone - plane.cpp - everything about the aircraft
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "global.hpp"
#include "plane.hpp"
#include "point.hpp"
#include "tables.hpp"
#include "map.hpp"
using namespace std;

// extern struct Map map;


Plane::Plane(int planetype, unsigned int posx, unsigned int posy):
	//initialize everything possible from the arguments
	planetype(planetype),
	posx(posx), posy(posy)
{
	//the constructor for the Plane class
	heading = N;
	update_points();
}


void Plane::draw(SDL_Color* col, int viewx, int viewy)
{
	//draw the plane using the colour and alpha defined by col
	vector<Sint16> bsx, bsy,
		csx, csy; //screen coordinates
	//convert map coordinates to screen coordinates
	for(unsigned int k=0; k<body.size(); k++)
	{
		bsx.push_back(body[k].x - viewx);
		bsy.push_back(body[k].y - viewy);
	}
	for(unsigned int k=0; k<canopy.size(); k++)
	{
		csx.push_back(canopy[k].x - viewx);
		csy.push_back(canopy[k].y - viewy);
	}

	if(col == NULL) //use default colours and maximum opacity
	{
		SDL_Color bc = body_cols[planetype], //body colour
				  cc = canopy_cols[planetype]; //canopy colour
		filledPolygonRGBA(renderer, bsx.data(), bsy.data(), bsx.size(),
				bc.r, bc.g, bc.b, bc.a);
		filledPolygonRGBA(renderer, csx.data(), csy.data(), csx.size(),
				cc.r, cc.g, cc.b, cc.a);
	}
	else //use the colour and opacity given
	{
		filledPolygonRGBA(renderer, bsx.data(), bsy.data(), bsx.size(),
				col->r, col->g, col->b, col->a);
		//for now, the canopy isn't drawn in this mode (may be changed later)
	}
}


void Plane::update_heading(int change)
{
	//increases or decreases heading based on change's sign
	if(change > 0)
		heading++;
	else if(change < 0)
		heading--;

	//headings go in a loop
	if(heading > NW)
		heading = N;
	else if(heading < N)
		heading = NW;

	update_points();
}


void Plane::update_points()
{
	//calculate point coordinates, taking into account the plane's heading
	//and the camera's position
	//first, clear all the points coordinates
	body.clear();
	canopy.clear();
	//engines.clear();
	//the size factor to multiply point coordinates by
	unsigned short int size_factor = 2;
	//process the points in loops
	//body
	vector<fPoint> b = mirror_vec(body_t[planetype]);
	for(unsigned int k=0; k<b.size(); k++)
	{
		//convert fPoint (float) to Point (Sint16)
		body.push_back(Point((Sint16)(b[k].x * size_factor), (Sint16)(b[k].y * size_factor)));
		//and rotate the point
		body[k].rotate(heading, posx, posy);
	}
	//canopy
	b.clear();
	b = mirror_vec(canopy_t[planetype]);
	for(unsigned int k=0; k<b.size(); k++)
	{
		//convert fPoint (float) to Point (Sint16)
		canopy.push_back(Point((Sint16)(b[k].x * size_factor), (Sint16)(b[k].y * size_factor)));
		//and rotate the point
		canopy[k].rotate(heading, posx, posy);
	}
	//engines
	/*
	for(unsigned int k=0; k<engine_t[planetype].size(); k++)
	{
		//old coordinates
		Point op = {(Sint16)(engine_t[planetype][k].x * size_factor),
			(Sint16)(engine_t[planetype][k].y * size_factor)};
		//place the updated point into body
		engines.push_back(rotate_point(op, heading, posx, posy));
	}
	*/
}


void Plane::update_pos()
{
	//calculates the new position of the plane from its speed and heading
	//the angle, measured from N, Pi/4 is 45 degrees
	float th = (M_PI / 4) * heading - M_PI / 2;
	//the projections of the speed vector onto x and y axis
	short int dx = ceil(speed * cosf(th)),
		dy = ceil(speed * sinf(th));
	//changing the coordinates
	posx += dx;
	posy += dy;
	//update the points' positions
	update_points();

	//check if any of the points are outside the map's boundaries
	unsigned short int left = 0, top = 0, right = 0, bottom = 0;
	for(auto k = body.begin(); k != body.end(); ++k)
	{
		if(k->x > map.w)
			right++;
		else if(k->x < 0)
			left++;
		if(k->y > map.h)
			bottom++;
		else if(k->y < 0)
			top++;
	}

	//XXX: the following can be written in a much shorter way if we store
	//the two components of the speed vector instead of its heading
	if(left > 0)
	{
		switch(heading)
		{
			case NW: heading = NE; break;
			case W: heading = E; break;
			case SW: heading = SE; break;
		}
	}
	else if(right > 0)
	{
		switch(heading)
		{
			case NE: heading = NW; break;
			case E: heading = W; break;
			case SE: heading = SW; break;
		}
	}
	if(top > 0)
	{
		switch(heading)
		{
			case NW: heading = SW; break;
			case N: heading = S; break;
			case NE: heading = SE; break;
		}
	}
	else if(bottom > 0)
	{
		switch(heading)
		{
			case SW: heading = NW; break;
			case S: heading = N; break;
			case SE: heading = NE; break;
		}
	}
	//now update the points once more
	update_points();
}
