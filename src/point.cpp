/*  openZone - point.hpp - point functions
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <SDL2/SDL.h>
#include "global.hpp"

using namespace std;


vector<fPoint> mirror_vec(vector<fPoint> vec)
{
	// mirrors the points (except the first and the last one) along the X axis
	// and adds the mirrored points to the vector
	vector<fPoint> res = vec;
	for(int k=vec.size() - 2; k>0; k--)
	{
		res.push_back({-vec[k].x, vec[k].y});
	}
	return res;
}

