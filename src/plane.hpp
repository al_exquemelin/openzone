/*  openZone - plane.hpp - the header file for plane.cpp
    Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include "point.hpp"


//plane types: Boomerang, DFA Bomber, Fauchard, Stiletto, Avenger
enum planetypes {GHOST, SWORDSMAN, GLAIVE, DIRK, RETALIATOR};

class Plane
{
	//a class for every aircraft in the game
	public:
		//the constructor
		Plane(int planetype, unsigned int posx, unsigned int posy);
		unsigned short int planetype, //the plane's type
					   posx, posy; //position
		//methods to change the plane's speed and heading
		void update_heading(int change);
		void update_speed(int change);
		//move the plane
		void update_pos();
		//calculate point coordinates, taking into account the plane's heading
		void update_points();
		void draw(SDL_Color* col, int viewx, int viewy); //draw the plane
		short int speed; //aircraft speed

	private:
		std::vector<Point> body, //silhouette points
			  canopy, //canopy points
			  engines; //engine exhaust points
		//Point structs contain Sint16  values that should fit the SDL2's filledPolygonRGBA;
		//point coordinates are _map_ coordinates, converted to screen ones
		//in the draw() function
		
		short int //speed, //aircraft speed
			  heading, //and heading
			  armor, //defines damage taken before going down
			  fuel; //fuel left
		//a float multiplier used to zoom the aircraft at landing and takeoff
		float alt;
};

