#   openZone - the Makefile
#   Copyright (C) 2019 Viktor Goryainov <al_exquemelin@yahoo.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or 
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, 
#   but WITHOUT ANY WARRANTY; without even the implied warranty of 
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License 
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

CC = g++
objects = openzone.o plane.o game.o point.o map.o
debug: copts = -Wall -std=gnu++11 -c -g -DDEBUG
openzone: copts = -Wall -std=gnu++11 -c

ifeq ($(OS), Windows_NT)
	copts += -DWINDOWS
	lopts = -lmingw32 -lSDL2 -lSDL2_gfx -mwindows
	exec = openzone.exe
else
	lopts = -lSDL2 -lSDL2_gfx
	exec = openzone
endif

vpath %.cpp src
vpath %.hpp src

openzone: $(objects)
	$(CC) $(objects) -Wall -o $@ $(lopts)

debug: $(objects)
	$(CC) $(objects) -Wall -g -o openzone $(lopts)

openzone.o: openzone.cpp game.hpp global.hpp
	$(CC) $(copts) $<

plane.o: plane.cpp plane.hpp global.hpp point.hpp tables.hpp map.hpp
	$(CC) $(copts) $<

game.o: game.cpp game.hpp global.hpp plane.hpp
	$(CC) $(copts) $<

point.o: point.cpp
	$(CC) $(copts) $<

map.o: map.cpp game.hpp global.hpp map.hpp point.hpp
	$(CC) $(copts) $<

clean: 
	-rm $(exec) $(objects)
